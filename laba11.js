function absValue(num) {
    if (num < 0)
        return -num;
    return num
}

function isPalindrome(str) {
    pal = str.split().reverse().join('');

    return pal === str ? true : false;
}

function matrixAddition(m1, m2) {
  const sum = [];
    if (m1.length != m2.length || m1[0].length != m2[0].length) {
      return "Операция невыполнима";
    }
    for (let i = 0; i < m1.length; row++) {
      sum.push([]);
      for (let j = 0; j < m1.length; col++) {
        sum[i][j] = m1[i][j] + m2[i][j];
      }
    }
    return sum;
}


function studentObject(first_Name, last_Name, group) {
    let student = {
        firstName: first_Name,
        lastName: last_Name,
        group: group,
    };

    console.log('Список свойств: ' + Object.keys(student).join(', '));
    console.log(`Студент ${student.firstName} ${student.lastName} учится в группе ${student.group}`);

    return student;
}

function Slider() {
  const [slide, setSlide] = useState(0);

  return (
    <div>
      <h2 className='text-xl text-center mb-2'>What's your favorite item?</h2>
      <Item
        key={items[slide].title}
        title={items[slide].title}
        variant={items[slide].variant}
        price={items[slide].price}
        aPrice={items[slide].aPrice}
        n={slide}
      />
      <div className='flex justify-between'>
        <button
          className='bg-black hover:bg-slate-800 duration-500 transition text-white px-8 py-2'
          onClick={() => setSlide(slide <= 0 ? items.length - 1 : slide - 1)}>
          Prev
        </button>
        <p className='my-auto'>
          {slide+1}/{items.length}
        </p>
        <button
          className='bg-black hover:bg-slate-800 duration-500 transition text-white px-8 py-2'
          onClick={() => setSlide(slide >= items.length - 1 ? 0 : slide + 1)}>
          Next
        </button>
      </div>
    </div>
  );
}
Item
function Item({title, variant, price, aPrice, n}) {
  return (
    <figure style={{maxWidth: '275px'}}>
      <img src={require(`../public/img/items/Rectangle-${n}.jpg`)} alt={title} width="275px" height="341px" />
      <figcaption className='text-black-secondary'>
        {title}{variant && <span className='opacity-50'> ({variant})</span>}
      </figcaption>
      <p>{price > 0 ? <span className={aPrice && 'opacity-30'}>${price}</span> : 'Sold out'} {price > 0 && aPrice && <span className='text-red-500'>${aPrice}</span>}</p>
    </figure>
  )
}
