function upr1() {
    console.log(typeof(9))
    // Предположение: number
    // Фактический: number
    
    console.log(typeof(1.2))
    // Предположение: number
    // Фактический: number
    
    console.log(typeof(NaN))
    // Предположение: number
    // Фактический: number
    
    console.log(typeof("Hello World"))
    // Предположение: string
    // Фактический: string
    
    console.log(typeof(true))
    // Предположение: boolean
    // Фактический:boolean
    
    console.log(typeof(2 != 1))
    // Предположение: boolean
    // Фактический: boolean
    
    
    console.log("сыр" + "ы")
    // Предположение: сыры
    // Фактический: сыры
    
    console.log("сыр" - "ы")
    // Предположение: NaN
    // Фактический: NaN
    
    console.log("2" + "4")
    // Предположение: 24
    // Фактический: 24
    
    console.log("2" - "4")
    // Предположение: -2
    // Фактический: -2
    
    console.log("Сэм" + 5)
    // Предположение:Сэм5
    // Фактический:Сэм5
    
    console.log("Сэм" - 5)
    // Предположение: NaN
    // Фактический: NaN
    
    console.log(99 * "шары")
    // Предположение:NaN
    // Фактический:NaN
}

function upr2 (a, b) {
    // * Найти площадь и периметр прмоугольника и их отношение
    const square = a * b
    const perimeter = (a + b) * 2
    const otn = square / perimeter

    console.log(`Площадь: ${square}, Периметер: ${perimeter}, Отношение ${otn}`)
}

function upr3(degrees){
    console.log(`
    ${degrees}°C соответствует ${degrees * 1.8 + 32}°F
    ${degrees}°F соответствует ${(degrees - 32) / 1.8}°С
    `)
}

function upr4(){
    const year = prompt('Введите год: ')
    if (year % 400 == 0) return alert('Год високосный');
    else if (year % 100 == 0) return alert('Год не високосный')
    else if (year % 4 == 0) return alert('Год високосный')
    else return alert('Год не високосный')
}

function upr5(a, b) {
    if (a + b == 10 || a == 10 || b == 10) console.log(true)
    else console.log(false)
}

function upr6(num) {
    let s = ''
    for (let i = 1; i <= num; i++) {
        s += `${i} овца... `
    }
    console.log(s)
}

function upr7 (num) {
    for (let i = 0; i < num; i++) {
        console.log(i % 2 ? `${i} нечет` : `${i} чет`)
    }
}

function upr8() {
    let s = ''
    for (let i = 1; i <= 12; i++) {
        s += i % 2 ? '*'.repeat(i) : '#'.repeat(i)
        s += '\n'
    }
    console.log(s)
}

function upr9(...nums) {
    console.log(nums.sort((a, b) => a - b).join(','))
}

function upr10(...nums) {
    console.log(Math.max(...nums))
}


