# АНАЛИЗ СОВРЕМЕННЫХ САЙТОВ

## ЛАБОРАТОРНАЯ РАБОТА №10 по дисциплине <Веб-технологии>

__Выполнила__ Студентка группы 211-329 Факуинги Д.Б.

## 10 сайтов

| название сайта    | ссылка                                          | краткое описание                                                                                                                                                                                                                                            |
|:----------------- |:-----------------------------------------------:| -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| Itech             | [itech](http://www.itech-group.ru/)             | ITECH — это IT-компания, которая помогает трансформировать бизнес и адаптировать его к цифровой среде. Мы создаем IT-решения для автоматизации бизнес-процессов. Разрабатываем и развиваем enterprise-решения, проекты в вебе и мобайле.                    |
| Art.LebedevStudio | [Art.LebedevStudio ](http://www.artlebedev.ru/) | Мы любим и умеем точно решать сложные бизнес-задачи больших и маленьких компаний и делать лучше повседневную жизнь людей — с помощью дизайнa.                                                                                                               |
| Notemedia         | [Notemedia](http://www.nota.media/)             | NOTAMEDIA — ЛИДЕР В СОЗДАНИИ ЦИФРОВЫХ СЕРВИСОВ И DIGITAL-ЭКОСИСТЕМ ДЛЯ БИЗНЕСА И ГОССТРУКТУР                                                                                                                                                                |
| ItConstruct       | [ItConstruct](http://www.itconstruct.ru/)       | Компания ITConstruct — веб-разработчик с 16-летним опытом работы. ITConstruct существует так долго, потому что мы обеспечиваем надёжность IT-решений и внедрений.                                                                                           |
| Liquid            | [Liquid](http://www.liquid.ru/)                 | Loading создает одежду для повседневной активной жизни, сочетая лучшие материалы и продуманный дизайн, делая акцент на функциональности и комфорте.                                                                                                         |
| ARTW              | [ARTWt](http://www.artw.ru/)                    | Компания «ARTW» является лидером крупной веб-разработки по Северо-Западу, членом координационного совета Содружества Петербургских Цифровых Агентств, неоднократным победителем таких ключевых и старейших конкурсов интернет-проектов                      |
| Далее             | [Далее](http://www.dalee.ru/)                   | ДАЛЕЕ — одно из крупнейших агентств на российском рынке веб-разработки, в штате работают более 100 человек: в Москве, Курске и Краснодаре.                                                                                                                  |
| Студия 15         | [Студия 15](http://www.15web.ru/)               | Разрабатываем и развиваем сайты и веб-сервисы на фреймворках Symfony Laravel; React, Next.js, Vue, Nuxt.js.Прозрачность, контролируемость, высокий уровень проектного менеджмента                                                                           |
| CreativePeoole    | [Creativepeople](http://www.cpeople.ru/)        | CreativePeople мы создаем дизайн и делаем это круто. Digital experience — это акцент на знаниях, которые мы накопили. Понимаем, как эмоции в digital становятся впечатлениями, а рациональный результат взаимодействия превращается в потребительский опыт. |
| PERX              | [PERX](http://www.perx.ru/)                     | Мы создаем уникальные сервисы для ведущих мировых автопроизводителей и дилерских сетей, помогая нашим клиентам стать лидерами цифровой трансформации.                                                                                                       |

## Свойства

| №   | Синтаксис,значения                                                        | Назначение                                                                                                       | Где встретилось(ссыдка на сайте)                |
| --- | ------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------- | ----------------------------------------------- |
| 1   | webkit-font-smoothing:[antialiased,none]                                  | позволяют управлять сглаживанием шрифта                                                                          | [itech](http://www.itech-group.ru)              |
| 2   | min-height:[100px;2em;50%;inherit;]                                       | Устанавливает минимальную высоту элемента                                                                        | [Art.LebedevStudio ](http://www.artlebedev.ru/) |
| 3   | max-width:[%;none;initial;inherit;]                                       | Устанавливает максимальную ширину элемента                                                                       | [Notemedia](http://www.nota.media/)             |
| 4   | border-bottom-right-radius:[length;%;initial;inherit]                     | Определяет форму границы нижнего правого угла                                                                    | [ItConstruct](http://www.itconstruct.ru/)       |
| 5   | border-top-left-radius:[length;%;initial;inherit]                         | Определяет форму границы в верхнем левом углу                                                                    | [Liquid](http://www.liquid.ru/)                 |
| 6   | cursor:[alias;all-scroll;auto;context-menu;grabbing,...]                  | задает вид курсора при нахождении его над элементом                                                              | [ARTW](http://www.artw.ru/)                     |
| 7   | min-width:[%;none;initial;inherit;]                                       | Устанавливает минимальную ширину элемента                                                                        | [Далее](http://www.dalee.ru/)                   |
| 8   | text-rendering:[auto;optimizeSpeed;optimizeLegibility;geometricPrecision] | предоставляет информацию механизму визуализации о том, что нужно оптимизировать при рендеринге текста            | [Студия15](http://www.15web.ru/)                |
| 9   | line-height:[normal;number;length;initial;inherit"]                       | устанавливает расстояние между базовыми линиями двух соседних строк                                              | [Creativepeople](http://www.cpeople.ru/)        |
| 10  | z-index:[auto,number,initial,inherit;]                                    | определяет порядок расположения позиционированных элементов по оси Z (порядок наложения элементов друг на друга) | [PERX](http://www.perx.ru/)                     |

## HTML Код

```<html>
<head> 
<title>...</title>
</head> 
<body>                  
 body {
position: relative;
transition: transform .3s;
scroll-behavior: smooth;
{
        margin: 0;
    padding: 0;
    border: 0;
    outline: 0;
    font-size: 100%;
    vertical-align: baseline;
    background: transparent
}
<script src="https://cloud.roistat.com/api/site/1.0/
5e6bf6f7eb7b2e9db52c52d43b8dde5c/get-phone?visit=427283&marker=:utm:ratingruneta.ru_referral_web&prefix=&prefix_bind=&phone_scripts_bind=&page=https%3A%2F%2Fitech-group.ru%2F%3Futm_source%3Dratingruneta.ru%26utm_medium%3Dreferral%26utm_campaign%3Dweb" type="text/javascript" async=""></script>
<div style="display: none;"</div>
<script>
<link rel= "apple-touoch-icon" sizes="180*180"...>
</div>
<meta property="og:site_name" content="ITECH — IT-компания полного цикла">
</body>
</html>
```
## Блок

<img src="Снимок%20экрана%20(509).png">
<img src="Снимок%20экрана%20(510).png">

![Screenshot](Снимок%20экрана%20(509).png)
![Screenshot2](Снимок%20экрана%20(510).png)
``` <div class="col-2 col-l-2 col-s-2 col-s-offset-2 other-clients__list">
    <div class="list-title">Телеком и IT:</div>
    <ul class="unmarked-list">
        <li>Билайн</li>
        <li>МТС</li>
        <li>МегаФон</li>
        <li>Сибинтек</li>
        <li>Лаборатория Касперского</li>
        <li>+10</li>
    </ul>
    <div class="list-title">Недвижимость:</div>
    <ul class="unmarked-list">
        <li>MR Group</li>
        <li>Capital Group</li>
        <li>ГАЛС Девелопмент</li>
        <li>Башня Федерация</li>
        <li>Mercury Tower</li>
        <li>+26</li>
    </ul>
    <div class="list-title">Банки и&nbsp; финансы:</div>
    <ul class="unmarked-list">
        <li>Райффайзенбанк</li>
        <li>Банк Хоум Кредит</li>
        <li>МТС Банк</li>
        <li>Альфа-Банк</li>
        <li>Tinkoff Bank</li>
        <li>+11</li>
        </ul>
    <div class="list-title">E-commerce:</div>
    <ul class="unmarked-list">
        <li>Юла</li>
        <li>Эконика</li>
        <li>Эльдорадо</li>
        <li>7 Цветов</li>
        <li>Спортдепо</li>
        <li>+6</li>
    </ul>
    <div class="list-title">Промышленность:</div>
    <ul class="unmarked-list">
        <li>Полюс</li>
        <li>Акрон</li>
        <li>Nordgold</li>
        <li>Евроцемент</li>
        <li>Ариэль Металл</li>
        <li>+5</li>
    </ul>
    <div class="list-title">Другие отрасли:</div>
    <ul class="unmarked-list">
        <li>S7</li>
        <li>Canon</li>
        <li>STADA</li>
        <li>R.O.C.S</li>
        <li>PepsiCo</li>
        <li>VitrA</li>
    </ul>
    </div>
```

## Нижний колонтитул.
1.у нас есть div (блочный элемент) для заголовка списка классов для телекоммуникаций и ИТ.
2.затем ul (для перечисления списка) для класса непомеченного списка
3.и после li (для перечисления элементов списка).
4.у нас есть второй div (блочный элемент) для класса list-title для недвижимости.
5.затем ul (для перечисления списка) для класса непомеченного списка
6.и после li (для перечисления элементов списка).
7.у нас все еще есть div (блочный элемент) для заголовка списка классов для банковского дела и финансов.
8.затем ul (для перечисления списка) для класса непомеченного списка
9.и после li (для перечисления элементов списка).
нижний колонтитул.
10.у нас есть div (блочный элемент) для заголовка списка классов для телекоммуникаций и ИТ.
11.затем ul (для перечисления списка) для класса непомеченного списка
12.и после li (для перечисления элементов списка).
13.у нас есть третий div (блочный элемент) для класса list-title для недвижимости.
14.атем ul (для перечисления списка) для класса непомеченного списка
и после li (для перечисления элементов списка).
у нас все еще есть div (блочный элемент) для заголовка списка классов для банковского дела и финансов.
затем ul (для перечисления списка) для класса непомеченного списка
и после li (для перечисления элементов списка)...

[Creativepeople](http://www.cpeople.ru/)

#### Ссылка на сайт

[itech](http://www.itech-group.ru/)

## Список из 10 интернет ресурсов

| №   | название сайта        | ссылка на сайте                                        | КОД                      |
| --- | --------------------- | ------------------------------------------------------ | ------------------------ |
| 1   | HTML academy          | [HTMLAcademy](http://www.htmlacademy.ru/)              | Html and CSS             |
| 2   | Skillbox              | [Skillbox](http://www.skillbox.ru/)                    | CSS                      |
| 3   | OPen TUTO             | [OpenTuto](http://www.gb.ru/)                          | Bootstrap                |
| 4   | Geekbrain             | [Geekbrain](http://www.gb.ru/)                         | Html                     |
| 5   | Openclassrooms        | [OpenClassrooms](http://www.openclassrooms.com/)       | Python, html, javascript |
| 6   | React                 | [React](http://ru.react.js.org/)                       | javasccript              |
| 7   | Coursera              | [Coursera](http://www.coursera.org/)                   | Python                   |
| 9   | Dash General Assembly | [Dash General Assembly](http://dash.generalassemb.ly/) | PHP                      |
| 10  | Codecademy            | [Codecademy](http://www.codecademy.com/)               | Html                     |